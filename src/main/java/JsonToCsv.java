import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.File;
import java.io.IOException;

public class JsonToCsv {

    private JsonNode arbre, element;
    private CsvSchema.Builder constSchemaCSV;
    private CsvSchema schemaCSV;
    private CsvMapper mapCSV;
    private File origine;

    public JsonToCsv(String path){

        origine = new File(path);

        try{
            arbre = new ObjectMapper().readTree(origine);
        }
        catch(IOException e){
            System.out.println("Erreur de lecture de fichier");
        }
        constSchemaCSV = CsvSchema.builder();
        element = arbre.elements().next();
        element.fieldNames().forEachRemaining(fieldName -> {constSchemaCSV.addColumn(fieldName);});
        schemaCSV = constSchemaCSV.build().withHeader();
        mapCSV = new CsvMapper();
        String nomFichier = origine.getName();
        nomFichier = nomFichier.substring(0, nomFichier.length() - 4);
        String newPath = "../Sorties/" + nomFichier + "csv";
        try {
            mapCSV.writerFor(JsonNode.class)
                    .with(schemaCSV)
                    .writeValue(new File(newPath), arbre);
        }
        catch(IOException e){
            System.out.println("Erreur d'ecriture de fichier");
        }

        System.out.println("Conversion de JSON vers CSV terminée. Fichier disponible à l'adresse : " + newPath + ".");
    }
}
