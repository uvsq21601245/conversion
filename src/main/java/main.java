import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.Scanner;

public class main {

    public static void main (String[] args) throws Exception {

        Scanner scan = new Scanner(System.in);

        /*
            System.out.println("Working Directory = " +
            System.getProperty("user.dir"));
        */

        System.out.println("Indiquer le nom du fichier a convertir.");
        System.out.println("Il doit etre present dans le dossier Origines");

        String path = scan.nextLine();
        String extension = FilenameUtils.getExtension(path);
        System.out.println(extension);

        File f = new File("../Origines/" + path);
        if(f.exists() && !f.isDirectory()) {
            if(extension.equals("csv")){
                csv_to_json cToJ = new csv_to_json("../Origines/" + path);
            }
            else if(extension.equals("json")){
                JsonToCsv jToC = new JsonToCsv("../Origines/" + path);
            }
            else{
                System.out.println("Type de fichier incompatible");
            }
        }
        else{
            System.out.println("Ce fichier n'existe pas.");
        }
    }
}
