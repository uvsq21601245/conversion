import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;


public class csv_to_json {

    public csv_to_json(){

    }

    public csv_to_json(String path) throws Exception {

        File input = new File(path);
        String nom = input.getName();
        nom = nom.substring(0, nom.length() - 3);
        File output = new File("../Sorties/" + nom + "json");

        List<Map<?, ?>> data = readObjectsFromCsv(input);

        output.createNewFile();

        writeAsJson(data, output);
    }

    public static List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {

        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        MappingIterator<Map<?, ?>> mappingIterator = mapper.reader(Map.class).with(schema).readValues(file);
        return mappingIterator.readAll();
    }

    public static void writeAsJson(List<Map<?, ?>> data, File file) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, data);
        System.out.println("Conversion de CSV vers JSON terminée.");
    }

}
