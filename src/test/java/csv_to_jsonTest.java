import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.*;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


public class csv_to_jsonTest {

    @Rule
    public TemporaryFolder tmp= new TemporaryFolder();



   @Test
   public void testConv() throws Exception {

       File json= new File("Origines/testJSON.json");
       File csv_converted= tmp.newFile("testJSON.json");

       csv_to_json test = new csv_to_json();
       File entree= new File("Origines/testCSV.csv");

       List<Map<?, ?>> data = csv_to_json.readObjectsFromCsv(entree);

       csv_to_json.writeAsJson(data, csv_converted);

       //assertEquals("Erreur fichier différent!", FileUtils.readFileToString(json, "utf-8"), FileUtils.readFileToString(csv_converted, "utf-8"));
       assertTrue("Erreur fichier différent!", FileUtils.contentEquals(json, csv_converted));

    }


}
