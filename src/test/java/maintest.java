import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class maintest {

    @Test
    public void testExtension(){
        String path = "fichierTest.json";
        assertEquals(FilenameUtils.getExtension(path), "json");
    }
}
