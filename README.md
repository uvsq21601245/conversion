Il faut faire la commande suivante dans le dossier pour ajouter maven wrapper au projet :
mvn -N io.takari:maven:0.7.7:wrapper

MANUEL UTILISATEUR

Lorsque le programme demande le nom du fichier à convertir, il suffit d'entrer le nom d'un fichier
JSON ou CSV présent dans le dossier "Origines" du projet puis d'appuyer sur Entrée.

Ex : "test.json" ou "test.csv"

Si le fichier existe et est de la bonne extension, il est convertit puis enregistré dans le dossier
"Sorties" du projet. Dans le cas contraire, l'éxecution du programme est interrompue.

A l'heure actuelle, on ne peut ni changer le nom des attributs en sorties, ni avoir d'attributs
calculables. Un champs en entrée est convertit en un champs en sortie, les deux champs ayant
le même nom.
